import pandas as pd
#import gmaps
from bokeh.io import curdoc, show
from bokeh.layouts import column
from bokeh.models import ColumnDataSource, Select, DatePicker, Button, GMapOptions
from bokeh.plotting import figure, gmap

# Configuración de API de mapas
map_options = GMapOptions(lat=4.64, lng=-74.06, map_type="roadmap", zoom=11)
#gmaps.configure(api_key='AIzaSyB6o5OyV-vgXK4dj1M0KTqYhpkHKc9D5f4')
p = gmap('AIzaSyB6o5OyV-vgXK4dj1M0KTqYhpkHKc9D5f4',title="Rappimapa",map_options=map_options)

# Read the data
order_df = pd.read_hdf('mongodump.hdf5')
# Cambio de indice a 'timestamp'
order_df_time = order_df.set_index('timestamp')

# Definición de función de filtro


def data_source(timein,timefin,service):
    """
    Regresa los datos filtrados según el servicio
    """
    order_df_time_r = order_df_time.loc[timein:timefin]
    order_df_time_serv = order_df_time[order_df_time['type']==service]
    order_df_time_serv_cord = order_df_time_serv[['lat','lng']]
    return order_df_time_serv_cord

#plot = gmaps.figure()

def plot_source(data_in,p):
    """
    Renderiza mapa de filtrado gracias a datos de entrada
    """
    # Adiciona capa de filtrado
    p.circle(x='lng',y='lat',size = 15,fill_color="blue",fill_alpha=0.8, source=data_in)

    plot = show(p)
    return plot

timein = '2018-09-05'
timefin = '2018-09-06'
service = 'restaurant'

source = ColumnDataSource(data_source(timein,timefin,service))


datetime_inicio = DatePicker(title = "Fecha Inicio", min_date = '2018-09-05',max_date = '2018-09-11', value = timein)
datetime_fin = DatePicker(title = "Fecha Final", min_date = '2018-09-05',max_date = '2018-09-11', value = timefin)
menu = Select(options = ['restaurant', 'ultraservicio', 'express_tablet', 'courier', 'whim', 'express', 'market'],
                value = service, title = "Tipo de Servicio")

def callbackdate1(atrr,old,new):
    timein = datetime_inicio.value
    #timefin = datetime_fin.value
    #service = menu.value
    source.data = data_source(timein,timefin,service)

def callbackdate2(atrr,old,new):
    timefin = datetime_fin.value
    source.data = data_source(timein,timefin,service)

def callbackmenu(atrr,old,new):
    service = menu.value

datetime_inicio.on_change('value',callbackdate1)
datetime_fin.on_change('value',callbackdate2)
menu.on_change('value',callbackmenu)

def main():
    data_in = data_source(timein,timefin,service)
    plot = plot_source(data_in,p)
    return plot
button = Button(label="Prendalo Mijo")

button.on_click(main)


layout = column(datetime_inicio,datetime_fin,p,menu,button)
curdoc().add_root(layout)

from django.apps import AppConfig


class PlotsbokehConfig(AppConfig):
    name = 'plotsbokeh'
